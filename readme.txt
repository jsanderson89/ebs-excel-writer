EBS Excel Writer
Version 1


In my previous office, a large portion of our job was recieving and
inputting form results. Many of these forms were created by state
organizations, so they often varied wildly in design. One form
specifically, the EBS Form, was both very long and of an unusual
design. This form would often take an extremely long time for my
coworkers to input, so I created this tool to make it much simpler.

The form itself was divided into 4 sections, each containing a 
different number of question. Each question required two seperate 
responses, which both had one of three possible responses- resulting
in 92 data points for each form, besides the identifying information
for each entry.

This program allowed my coworkers to simply type a string of numbers
corresponding to the responses, then quickly add them to an Excel
spreadsheet.

Due to being unable to distribute copies of the form along with this
program, I am providing information on the source form to allow the
program to be properly evaluated.


Section Information of Source Form:
	"School Wide System" - 18 questions, 36 data points
	"Nonclassroom Setting Systems" - 9 questions, 18 data points
	"Classroom Systems" - 11 questions, 22 data points
	"Individual Student Systems" - 8 questions, 16 data points


Usage Guide:

Inputting Data:
	-Under the first section, type the number 1,2,or 3 a total of
	 thirty-six(36) times.

	-Under the second section, type the number 1,2,or 3 a total of
	 eighteen(18) times.

	-Under the third section, type the number 1,2,or 3 a total of
	 twenty-two(22) times.

	-Under the fourth section, type the number 1,2,or 3 a total of
	 sixteen(16) times.

	-Input the name of the school, name of the teacher, and date
	 of survey.

	-Click "Add EBS" to add the current form data to an internal
	 queue.

Outputting Excel File:

	-Click "Select Path" and browse to the target folder.
	
	-Input the filename for the Excel file.

	-Click "Save File to export the Excel Document.

	*Note that Save operations append the EBS data to the Excel file
	 selected rather than creating a new copy.
 

Current Issues:
	-Unhandled exception when the target file is open and the
	 program attempts to save.

	-There is no response to the user when an EBS entry is added,
	 nor then the file is exported.


This project uses the "ClosedXML" library, which is included.
"ClosedXML" can be found at: https://closedxml.codeplex.com/


Copyright Jesse Anderson, 2012-2014.