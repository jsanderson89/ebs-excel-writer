﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExcelWriter
{
    public partial class Form1 : Form
    {

        private string _folderPath = " ";
        private string _filename;
        public int test = 2;
        ExcelProcessor processor;
                       

        public Form1()
        {
            InitializeComponent();
            processor = new ExcelProcessor();
        }

        //Add EBS
        private void button1_Click(object sender, EventArgs e)
        {
            DateTime date = new DateTime();
            if (DateTime.TryParse(textBox7.Text, out date))
            {
                processor.WriteRowToTable(date, textBox5.Text, textBox6.Text, textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text);
            }
            else
            {
                MessageBox.Show("Invalid Date.");
            }
            
        }


        //Open Browser Dialog
        private void button2_Click(object sender, EventArgs e)
        {
            _folderPath = OpenFolderBrowser();
            textBox8.Text = _folderPath;
        }

        private string OpenFolderBrowser()
        {
            string returnVar = " ";
            DialogResult r = folderBrowserDialog1.ShowDialog();
            if (r == DialogResult.OK)
            {
                returnVar = folderBrowserDialog1.SelectedPath.ToString();
            }

            return returnVar;
        }

        //Save File
        private void button3_Click(object sender, EventArgs e)
        {
            string saveName;

            if (_folderPath != " ")
            {
                saveName = _folderPath + "\\" + textBox9.Text + ".xlsx";
            }

            else
            {
                saveName = textBox9.Text + ".xlsx";
            }

            processor.Save(saveName);
        }
    }
}
