﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClosedXML.Excel;

namespace ExcelWriter
{
    class ExcelProcessor
    {
        private DataTable table;

        public ExcelProcessor()
        {
            CreateTable();
        }

        private void CreateTable()
        {
            table = new DataTable();
            table.Columns.Add("Timestamp",typeof(DateTime));

            AddHeaderToTable(table,"[School Wide Systems]",1,18);
            AddHeaderToTable(table,"[Nonclassroom Setting Systems]", 2, 9);
            AddHeaderToTable(table,"[Classroom Systems]", 3, 11);
            AddHeaderToTable(table, "[Individual Student Systems]", 4, 8);
            table.Columns.Add("Teacher Name", typeof(String));

        }

        private void AddHeaderToTable(DataTable targetTable, string header, int number, int numberOfQuestions)
        {
            targetTable.Columns.Add(header, typeof(string));
            for (int i = 1; i <= numberOfQuestions; i++)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(number).Append(".").Append(i).Append(". Current Status");
                targetTable.Columns.Add(builder.ToString(), typeof(string));

                builder = new StringBuilder();
                builder.Append(number).Append(".").Append(i).Append(". Priority For Improvement");
                targetTable.Columns.Add(builder.ToString(), typeof(string));
            }

            table.Columns.Add("Name of School (" + number +")", typeof(string));
            table.Columns.Add("Date (" + number +")", typeof(DateTime));
        }
        public void WriteRowToTable(DateTime date, string nameOfSchool, string nameOfTeacher, string data_SWS, string data_NCSS, string data_CS, string data_ISS)
        {

            DataRow row = table.NewRow();
            List<string> sws = ProcessDataHeader(table, data_SWS, 18);
            List<string> ncss = ProcessDataHeader(table, data_NCSS, 9);
            List<string> cs = ProcessDataHeader(table, data_CS, 11);
            List<string> iss = ProcessDataHeader(table, data_ISS, 8);

            int dataRowIndexer = 0;

            row[dataRowIndexer] = DateTime.Now;
            dataRowIndexer++;

            row[dataRowIndexer] = "School Wide Systems";
            dataRowIndexer++;

            foreach(string i in sws)
            {
                row[dataRowIndexer] = i;
                dataRowIndexer++;
            }

            row[dataRowIndexer] = nameOfSchool;
            dataRowIndexer++;

            row[dataRowIndexer] = date;
            dataRowIndexer++;

            row[dataRowIndexer] = "Nonclassroom Setting Systems";
            dataRowIndexer++;

            foreach (string i in ncss)
            {
                row[dataRowIndexer] = i;
                dataRowIndexer++;
            }

            row[dataRowIndexer] = nameOfSchool;
            dataRowIndexer++;

            row[dataRowIndexer] = date;
            dataRowIndexer++;

            row[dataRowIndexer] = "Classroom Systems";
            dataRowIndexer++;

            foreach (string i in cs)
            {
                row[dataRowIndexer] = i;
                dataRowIndexer++;
            }

            row[dataRowIndexer] = nameOfSchool;
            dataRowIndexer++;

            row[dataRowIndexer] = date;
            dataRowIndexer++;

            row[dataRowIndexer] = "Individual Student Systems";
            dataRowIndexer++;

            foreach (string i in iss)
            {
                row[dataRowIndexer] = i;
                dataRowIndexer++;
            }

            row[dataRowIndexer] = nameOfSchool;
            dataRowIndexer++;

            row[dataRowIndexer] = date;
            dataRowIndexer++;

            row[dataRowIndexer] = nameOfTeacher;
            dataRowIndexer++;

            table.Rows.Add(row);
        }
        private List<string> ProcessDataHeader(DataTable targetTable, string sourceString, int numberOfQuestions)
        {
            List<string> returnList = new List<string>();

            if (sourceString.Length >= numberOfQuestions*2)
            {
                for (int i = 0; i+1 <= numberOfQuestions*2; i++)
                {

                    if (i % 2 == 0)
                    {
                        string valueOne = CheckCurrentStatus(sourceString[i]);
                        returnList.Add(valueOne);
                    }
                    else
                    {
                        string valueTwo = CheckPriorityForImprovement(sourceString[i]);
                        returnList.Add(valueTwo);
                    }
                }
            }

            else
            {
                for (int i = 0; i+1 <= sourceString.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        string valueOne = CheckCurrentStatus(sourceString[i]);
                        returnList.Add(valueOne);
                    }
                    else
                    {
                        string valueTwo = CheckPriorityForImprovement(sourceString[i]);
                        returnList.Add(valueTwo);
                    }
                }

                int numberToFill = (numberOfQuestions * 2) - sourceString.Length;
                FillBlankCells(returnList, numberToFill);
            }

            return returnList;

        }
        private string CheckCurrentStatus(Char value)
        {
            switch (value)
            { 
                case '1':
                    return "In Place";
                case '2':
                    return "Partial In Place";
                case '3':
                    return "Not In Place";
                default:
                    return " ";
            }
        }
        private string CheckPriorityForImprovement(Char value)
        {
            switch (value)
            {
                case '1':
                    return "High";
                case '2':
                    return "Med";
                case '3':
                    return "Low";
                default:
                    return " ";
            }
        }
        private void FillBlankCells(List<string> targetList, int numberOfBlankCells)
        {
            int i = 1;
            while (i <= numberOfBlankCells)
            {
                targetList.Add(" ");
                i++;
            }
        }

        public void Save(String filename)
        {
            
            var workbook = new XLWorkbook();
            workbook.Worksheets.Add(table, "Sheet 1");
            workbook.SaveAs(filename);

            CreateTable();
        }

        public void dev_HelloWorld()
        {
            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("Sample");
            worksheet.Cell("a1").Value = "Hello World!";
            workbook.SaveAs("HelloWorld.xlsx");
        }

    }
}
